#!/usr/bin/env python3

import logging
import os
import traceback

from programaker_bridge import (BlockArgument, BlockContext,
                                CallbackBlockArgument, CollectionBlockArgument,
                                MessageBasedServiceRegistration,
                                ProgramakerBridge, VariableBlockArgument)
from programaker_telegram_service import TelegramBot, assets, config, storage


class Registerer(MessageBasedServiceRegistration):
    def __init__(self, bot, bridge, *args, **kwargs):
        MessageBasedServiceRegistration.__init__(self, *args, **kwargs)
        self.bot = bot
        self.connection_strings = {}
        self.bridge = bridge

    def get_call_to_action_text(self, extra_data):
        if not extra_data:
            return 'Just greet <a href="https://telegram.me/{bot_name}">{bot_name}</a>'.format(
                bot_name=self.bot.bot_name
            )

        connection_id = extra_data.user_id
        self.connection_strings[connection_id] = connection_id

        return (
            '<a href="https://t.me/{bot_name}?start={user_id}">Click here if you have the Telegram App</a>. \n'
            'Otherwise, send the following to <a href="https://telegram.me/{bot_name}">{bot_name}</a>'
            "<console>/register {user_id}</console>".format(
                bot_name=self.bot.bot_name,
                user_id=connection_id,
            )
        )

    def perform_side_authentication(self, keyword, username):
        if not keyword in self.connection_strings:
            return False

        connection_id = self.connection_strings[keyword]
        del self.connection_strings[keyword]

        self.bridge.establish_connection(connection_id, name=username)
        return True


bot_token = config.get_bot_token()
bot_name = config.get_bot_name()
AUTH_TOKEN = config.get_auth_token()

BOT = TelegramBot(bot_token, bot_name)
STORAGE = storage.get_engine()

bridge = ProgramakerBridge(
    name="Telegram",
    is_public=True,
    events=[
        "on_new_message",
        "on_new_message_from_channel",
        "on_anyone_says_in_channel",
    ],
    collections=["channels"],
    icon=assets.open_icon(),
    token=AUTH_TOKEN,
    allow_multiple_connections=False,
)

REGISTERER = Registerer(bot=BOT, bridge=bridge)
bridge.registerer = REGISTERER

# Define events
CHANNEL_COLLECTION = bridge.collections.channels
on_new_message_event = bridge.events.on_new_message
on_new_message_event.add_trigger_block(
    id="on_new_message",
    message="When I say something in any channel. Set %1",
    arguments=[VariableBlockArgument(str)],
    save_to=BlockContext.ARGUMENTS[0],
)
on_new_message_event.add_trigger_block(
    id="on_command",
    message="When I say %1 in any channel",
    arguments=[BlockArgument(str, "/start")],
    expected_value=BlockContext.ARGUMENTS[0],
)

on_new_message_from_channel_event = bridge.events.on_new_message_from_channel
on_new_message_from_channel_event.add_trigger_block(
    id="on_new_message_from_channel",
    message="When I say something on channel %1. Set %2",
    arguments=[CollectionBlockArgument(CHANNEL_COLLECTION), VariableBlockArgument(str)],
    subkey=BlockContext.ARGUMENTS[0],
    save_to=BlockContext.ARGUMENTS[1],
)
on_new_message_from_channel_event.add_trigger_block(
    id="on_command_from_channel",
    message="When I say %1 on %2",
    arguments=[
        BlockArgument(str, "/start"),
        CollectionBlockArgument(CHANNEL_COLLECTION),
    ],
    expected_value=BlockContext.ARGUMENTS[0],
    subkey=BlockContext.ARGUMENTS[1],
)

on_anyone_says_in_channel_event = bridge.events.on_anyone_says_in_channel
on_anyone_says_in_channel_event.add_trigger_block(
    id="on_anyone_says_in_channel",
    message="When ANYONE says %1 on channel %2",
    arguments=[
        BlockArgument(str, "/start"),
        CollectionBlockArgument(CHANNEL_COLLECTION),
    ],
    expected_value=BlockContext.ARGUMENTS[0],
    subkey=BlockContext.ARGUMENTS[1],
)


# Define helper function
def get_chat_name(chat):
    if chat.title is not None:
        return chat.title
    if chat.username is not None:
        return chat.username
    logging.error("Unknown chat name from: {}".format(chat))
    return "chat-{}".format(chat.id)


@CHANNEL_COLLECTION.getter
def get_known_channels(user_id, extra_data):
    results = {}
    for (
        _telegram_user,
        telegram_room_id,
        telegram_room_name,
    ) in STORAGE.get_telegram_rooms_for_programaker_user(user_id):
        results[telegram_room_id] = {"name": telegram_room_name}
    return results


def on_new_message(update):
    if update.message is None:
        return

    user = update.message.from_user.id
    room = update.message.chat.id

    # Check for registration
    if check_message_for_registration(update, user, room):
        chat_name = get_chat_name(update.message.chat)
        STORAGE.add_user_to_room(user, room, room_name=chat_name)
        BOT.send(
            room,
            "Welcome! You're registered now!\n"
            "Now you can use this bot in PrograMaker.",
        )

    # Route the message depending on if the user is already registered
    if not STORAGE.is_telegram_user_registered(user):
        on_non_registered_event(user, room, update)
    else:
        # If the user is registered, allow it to send messages to this chat
        chat_name = get_chat_name(update.message.chat)
        STORAGE.add_user_to_room(user, room, room_name=chat_name)

        # And send the event notifying this message's reception
        for programaker_user in STORAGE.get_programaker_users_from_telegram(user):
            on_new_message_event.send(
                to_user=programaker_user,
                content=update.message.text,
                event=update.to_dict(),
            )
            on_new_message_from_channel_event.send(
                to_user=programaker_user,
                content=update.message.text,
                event=update.to_dict(),
                subkey=str(room),
            )

    # Sent in channel, make it available to all users on that channel
    if user != room:
        for programaker_user in STORAGE.get_programaker_users_in_room(room):
            on_anyone_says_in_channel_event.send(
                to_user=programaker_user,
                content=update.message.text,
                event=update.to_dict(),
                subkey=str(room),
            )


BOT.on_message = on_new_message


def check_message_for_registration(update, user, room):
    msg = update.message.text

    prefixes = ("/register ", "/start ")

    if msg:
        for prefix in prefixes:
            if msg.startswith(prefix):
                register_id = msg[len(prefix) :]
                if REGISTERER.perform_side_authentication(
                    register_id, update.message.from_user.name
                ):
                    STORAGE.register_user(user, register_id)
                    return True

    return False


def on_non_registered_event(user, room, update):
    if update.message.text is None:
        return

    if user != room:
        logging.info("Not sending registration offer, message received from a group")

    else:
        BOT.send(
            room,
            "Hi! I'm a bot in the making, ask @{maintainer} for more info if you want to know how to program me ;).".format(
                maintainer=config.get_maintainer_telegram_handle()
            ),
        )


@bridge.operation(
    id="send_message",
    message="On channel %1 say %2",
    arguments=[
        CollectionBlockArgument(CHANNEL_COLLECTION),
        BlockArgument(str, "Hello"),
    ],
)
def send_message(room_id, message, extra_data):
    BOT.send(room_id, message)


@bridge.operation(
    id="send_image_to_channel",
    message="On channel %1 send image %2 with text %3",
    arguments=[
        CollectionBlockArgument(CHANNEL_COLLECTION),
        BlockArgument(str, "Url to image"),
        BlockArgument(str, "Hello"),
    ],
)
def send_image_to_channel(room_id, url_to_image, message, extra_data):
    BOT.send_photo(chat_id=room_id, photo=url_to_image, message=message)


@bridge.operation(
    id="answer_message", message="Respond %1", arguments=[BlockArgument(str, "Hello")]
)
def answer_message(message, extra_data):
    if extra_data.last_monitor_value is None:
        raise Exception("Answer_message without previous call")

    last_room_id = (
        extra_data.last_monitor_value.get("message", {}).get("chat", {}).get("id", None)
    )
    if last_room_id is None:
        raise Exception(
            "Cannot call answer_message when last_message is “{}”".format(
                extra_data.last_monitor_value
            )
        )

    BOT.send(last_room_id, message)

@bridge.operation(
    id="answer_image",
    message="Respond with image %1 and text %2",
    arguments=[
        BlockArgument(str, "Url to image"),
        BlockArgument(str, "Hello"),
    ],
)
def answer_image(url_to_image, message, extra_data):
    if extra_data.last_monitor_value is None:
        raise Exception("Answer_image without previous call")

    last_room_id = (
        extra_data.last_monitor_value.get("message", {}).get("chat", {}).get("id", None)
    )
    if last_room_id is None:
        logging.error(
            "Cannot call answer_image when last_message is “{}”".format(
                extra_data.last_monitor_value
            )
        )
        return

    BOT.send_photo(chat_id=last_room_id, photo=url_to_image, message=message)



if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s - %(levelname)s [%(filename)s] %(message)s")
    logging.getLogger().setLevel(logging.INFO)

    bridge.endpoint = config.get_bridge_endpoint()
    bridge.on_ready = BOT.start
    try:
        bridge.run()
    except:
        traceback.print_exc()
        os._exit(1)

    os._exit(1)  # Force stopping after the bridge ends
